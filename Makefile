run: build
	java org.antlr.v4.gui.TestRig Bison file example.y -gui -tokens

build: BisonLexer.java BisonParser.java
	javac *.java

BisonLexer.java: BisonLexer.g4
	java -jar /usr/local/lib/antlr-4.6-complete.jar BisonLexer.g4

BisonParser.java: BisonParser.g4
	java -jar /usr/local/lib/antlr-4.6-complete.jar BisonParser.g4

clean:
	rm -f *.java *.class *.tokens
